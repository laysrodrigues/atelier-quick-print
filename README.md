# Atelier Plasmoid

An applet so you can do quick prints from
your KDE Plasma environment. =D

![Atelier Plasmoid](images/plasmoid.png)

## Pre-requisites

Have [AtCore](https://invent.kde.org/kde/atcore/) installed

## Development

To run this app on the development (without installing):

```bash
sudo apt install plasma-sdk
git clone git@invent.kde.org:laysrodrigues/atelier-plasmoid.git
cd atelier-plasmoid/plasmoid
plasmoidviewer --applet plasmoid
```

### Common Errors

- The plasmoid can't find AtCore:
    - You don't have installed on your `$PATH`
    - You didn't run `prefix.sh` on your custom install path of AtCore

## Add on your plasma desktop

### Part 01

```bash
git clone git@invent.kde.org:laysrodrigues/atelier-plasmoid.git
cd atelier-plasmoid
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$HOME/.local  
make install
```

### Part 02

- Click with the right button on your desktop
- Click `Add Widgets`
- Find Atelier on the list
- Drag and drop to your Plasma desktop
- Start to use it!

## Known Bugs

- The progress update don't work (Maybe bug on AtCore?)