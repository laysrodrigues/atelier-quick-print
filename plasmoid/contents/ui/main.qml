/**
 * Copyright (C) 2019 Lays Rodrigues <lays.rodrigues@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.11
import org.kde.atcore 1.0
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.plasmoid 2.0

ColumnLayout {
  property string printFile: ""
  readonly property int tempStepSize: 5

  AtCore {
    id: core
  }

  RowLayout {
    PlasmaComponents.Label {
      text: i18n("Atelier Quick Print")
      Layout.alignment: Qt.AlignHCenter
    }

    PlasmaComponents.Label {
      Layout.fillWidth: true
    }

    PlasmaComponents.Label {
      text: i18n("Status:")
    }

    PlasmaComponents.Label {
      id: status
      text: i18n("Disconnected")
    }
  }

  GridLayout {
    columns: 3

    PlasmaComponents.Label {
      text: i18n("Port")
    }

    QQC2.ComboBox {
      id: port
      model: core.serialPorts
    }

    QQC2.Button {
      id: connectBtn
      checkable: true
      text: checked ? i18n("Disconnect!") : i18n("Connect!")

      onClicked: {
        if(checked) {
          if (!core.newConnection(port.currentText, baud.currentText, firmware.currentText) ) {
            connectMsgError.open()
          }
        }
        else {
          core.closeConnection()
          selectFileBtn.enabled = false
        } 
      }
    }

    PlasmaComponents.Label {
      text: i18n("Profile")
    }

    QQC2.ComboBox {
      id: profiles
      model: MachineInfo.profileNames
      Layout.columnSpan: 2
      onActivated: {
        loadProfile(profiles.currentText)
      }
    }

    PlasmaComponents.Label{
      text: i18n("Baud")
    }

    QQC2.ComboBox{
      id: baud
      model: core.portSpeeds
      Layout.columnSpan: 2
    }

    PlasmaComponents.Label{
      text: i18n("Firmware")
    }

    QQC2.ComboBox{
      id: firmware
      model: core.availableFirmwarePlugins
    }
  }

  RowLayout {
    id: temperaturesLayout
    enabled: false
    Layout.fillWidth: true

    PlasmaComponents.Label {
      text: i18n("Bed")
    }

    QQC2.SpinBox{
      id: bedTargetTemperature
      editable: true
      from: 0
      to: 130
      stepSize: tempStepSize
      value: parseInt(core.temperature.bedTargetTemperature)
      onValueChanged: parseInt(core.setBedTemp(bedTargetTemperature.value))
    }

    PlasmaComponents.Label {
      text: "%1 ºC".arg(parseInt(core.temperature.bedTemperature))
    }

    PlasmaComponents.Label {
      Layout.fillWidth: true
    }

    PlasmaComponents.Label {
      text: i18n("Extruder")
    }

    QQC2.SpinBox {
      id: extruderTargetTemperature
      editable: true
      from: 0
      to: 240
      stepSize: tempStepSize
      value: parseInt(core.temperature.extruderTargetTemperature)
      onValueChanged: parseInt(core.setExtruderTemp(extruderTargetTemperature.value))
    }

    PlasmaComponents.Label {
      text: "%1 ºC".arg(parseInt(core.temperature.extruderTemperature))
    }
  }

  RowLayout {
    QQC2.Button {
      id: selectFileBtn
      enabled: false
      icon.name: "document-open"
      text: i18n("Select a File")
      onClicked: dialog.open()
    }

    PlasmaComponents.Label{
      text: printFile
    }
  }

  RowLayout {
    id: printJobLayout
    enabled: false
    PlasmaComponents.Label {
      text: i18n("Print Job")
    }

    QQC2.Button {
      icon.name: "process-stop"
      hoverEnabled: true
      onClicked: core.stop()
      QQC2.ToolTip.visible: hovered
      QQC2.ToolTip.text: i18n("Stop the print job.")
    }

    QQC2.Button {
      icon.name: "media-playback-pause"
      hoverEnabled: true
      checkable: true
      onClicked: checked ? core.pause("") : core.resume()
      QQC2.ToolTip.visible: hovered
      QQC2.ToolTip.text: i18n("Pause the print job.")
    }

    PlasmaComponents.Label {
      text: i18n("Progress")
    }

    PlasmaComponents.Label {
      id: progress
      text: "%1 %".arg(parseInt(core.percentagePrinted))
    }
    Component.onCompleted: {
      core.setSerialTimerInterval(2000)
      loadProfile(profiles.currentText)
    }
  }

 FileDialog{
    id: dialog
    title: i18n("Please, select a GCode")
    folder: shortcuts.home
    nameFilters: ["GCode Files (*.gcode *.gco)"]
    onAccepted: {
      var fileUrlToString = dialog.fileUrl.toString()
      printFile = fileUrlToString.replace(/^(file:\/{2})/, "")
      core.print(printFile)
      printJobLayout.enabled = true
    }

    onRejected: {
      printFile = ""
    }
  }

  Connections {
    target: core
    onStateChanged: {
      var st = ""
      switch (core.state) {
        case AtCore.CONNECTING:
          st = i18n("Connecting")
          break
        case AtCore.IDLE:
          st = i18n("Idle")
          printFile = ""
          selectFileBtn.enabled = true
          temperaturesLayout.enabled = true
          break
        case AtCore.BUSY:
          st = i18n("Printing")
          break
        case AtCore.ERRORSTATE:
          st = i18n("Error")
          break
        case AtCore.STOP:
          st = i18n("Stop")
          break
        case AtCore.PAUSE:
          st = i18n("Paused")
          break
        case AtCore.DISCONNECTED:
          st = i18n("Disconnected")
          printJobLayout.enabled = false
          temperaturesLayout.enabled = false
          break
      }
      status.text = st
    }
    onPortsChanged: {
      port.model = core.serialPorts
    }
    onPrintProgressChanged: {
      progress.text = "%1 %".arg(parseInt(core.percentagePrinted))
    }
  }

  MessageDialog{
    id: connectMsgError
    title: i18n("Error while connecting")
    text: i18n("There was an error while trying to connect to your 3DPrinter. \n Please check the selected values and try again.")
    onAccepted: {
      connectBtn.checked(false)
      Qt.quit()
    }
  }

  function loadProfile(profile) {
    baud.currentIndex = baud.indexOfValue(MachineInfo.readKey(profile, MachineInfo.BAUDRATE))
    bedTargetTemperature.to = MachineInfo.readKey(profile, MachineInfo.MAXBEDTEMP)
    extruderTargetTemperature.to = MachineInfo.readKey(profile, MachineInfo.MAXEXTTEMP)
    firmware.currentIndex = firmware.indexOfValue(MachineInfo.readKey(profile, MachineInfo.FIRMWARE))
  }
}
